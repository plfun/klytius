import Klytius
import Klytius.Show
import Klytius.PGrafo
import Strat

tparapp :: (Show a) => (a -> a) -> [a] -> TPar [a] -- Paralelo con il progetto
tparapp f [] = mkVars []
tparapp f (x:xs) = par fx ((mkVar' "(:)" (:)) <*> fx <*> (tparapp f xs))
    where
        fx = mkVars $ f x

parseqapp :: (Show a) => (a -> a) -> [a] -> TPar [a] -- Par, seq, con il progetto
parseqapp f [] = mkVars []
parseqapp f (x:xs) = par (pseq fx fx) ((mkVar' "(:)" (:)) <*> fx <*> (parseqapp f xs))
    where
        fx = mkVars $ f x

ej = [23,4,79014] :: [Int]
vtparapp = (tparapp (+1) ej)

tposMaybe :: (Show a) => Maybe a -> TPar ()
tposMaybe Nothing = mkVar' "()" ()
tposMaybe (Just x)= pseq (mkVars x) (mkVar' "()" ())

tmaybeapp :: (Show a) => (Maybe a -> Maybe a) -> [Maybe a] -> TPar [Maybe a]
tmaybeapp f [] = mkVars []
tmaybeapp f (x:xs) = par (tposMaybe =<< fx) ((mkVar' "(:)" (:)) <*> fx <*> (tmaybeapp f xs)) 
    where
        fx = mkVars (f x)

parmapstr :: (Show b, NFData b) => (a -> b) -> [a] -> TPar [b]
parmapstr f xs = map f xs `using` parList rdeepseq 

genparmap :: (NFData b, Functor f) => (Strategy b -> Strategy (f b)) -> (a -> b) -> f a -> TPar (f b)
genparmap st f xs = fmap f xs `using` st rdeepseq 

main = do
    graficar False vtparapp "parmap23479014"
    graficar False (tparapp (fmap (+1)) [Just 23, Just 4, Just 79014]) "parmapmaybe23479014"
    graficar True (tparapp (fmap (+1)) [Just 23, Just 4, Just 79014]) "parmaybeVerbose"
    graficar False (tmaybeapp (fmap (+1)) [Just 23, Just 4, Just 79014]) "maybeapp"
