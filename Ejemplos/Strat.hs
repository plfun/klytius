-----------------------------------------------------------------------------
---- |
---- Module      :  Control.Parallel.Strategies
---- Copyright   :  (c) The University of Glasgow 2001-2010
---- License     :  BSD-style (see the file libraries/base/LICENSE)
---- 
---- Maintainer  :  libraries@haskell.org
---- Stability   :  experimental
---- Portability :  portable
----
---- Parallel Evaluation Strategies, or Strategies for short, provide
---- ways to express parallel computations.  Strategies have the following
---- key features:
----
----  * Strategies express /deterministic parallelism/:
----    the result of the program is unaffected by evaluating in parallel.
----    The parallel tasks evaluated by a Strategy may have no side effects.
----    For non-deterministic parallel programming, see "Control.Concurrent".
----
----  * Strategies let you separate the description of the parallelism from the
----    logic of your program, enabling modular parallelism.  The basic idea
----    is to build a lazy data structure representing the computation, and
----    then write a Strategy that describes how to traverse the data structure
----    and evaluate components of it sequentially or in parallel.
----
----  * Strategies are /compositional/: larger strategies can be built
----    by gluing together smaller ones.
----
----  * 'Monad' and 'Applicative' instances are provided, for quickly building
----    strategies that involve traversing structures in a regular way.
---- 
---- For API history and changes in this release, see "Control.Parallel.Strategies#history".
--
-------------------------------------------------------------------------------

{-# LANGUAGE DeriveDataTypeable, RankNTypes #-}
module Strat where

import Klytius
import Klytius.Show
import Klytius.PGrafo
import Data.Traversable
import Data.Reify
----

type Strategy a = a -> TPar a

class NFData a where
    rnf :: a -> TPar ()
    rnf x = seqs "rnfGEn" (mkVar x) (mkVar' "()" ())

instance NFData Int where
    rnf x = seqs "rnfInt" (mkVars x) (mkVar' "()" ())

instance NFData a => NFData [a] where
    rnf [] = mkVar' "()" ()
    rnf (x:xs) = seqs "hd:" (rnf x) (rnf xs)

instance (NFData a, NFData b) => NFData (a,b) where
    rnf (a,b) = pseq (rnf a) (rnf b)

instance NFData a => NFData (Maybe a) where
    rnf Nothing = mkVar' "Nothing" ()
    rnf (Just x) = vlbl "Just" (rnf x)

instance (NFData a) => NFData (TPar a) where
    rnf x = x >>= rnf

dot :: Strategy a -> Strategy a -> Strategy a
dot str2 str1 x = str2 =<< (str1 x)

rpar :: Strategy a
rpar x = par x' x'
    where
        x' = mkVar x

rseq :: Strategy a
rseq x = pseq x' x'
    where
        x' = mkVar x

rdeepseq :: NFData a => Strategy a
rdeepseq x = seqs "deep" (rnf =<< x') x'
    where
        x' = mkVar x

r0 :: Strategy a
r0 x = mkVar x 

rwhnf :: Strategy a
rwhnf x = mkVar x

evalList :: Strategy a -> Strategy [a]
evalList s [] = return []
evalList s (x:xs) = do
    x' <- s x
    xs' <- evalList s xs
    return (x' : xs')

seqList :: Strategy a -> Strategy [a]
seqList _ [] = mkVar' "[]" []
seqList es (x:xs) = pseq (es =<< x') (mkVar' "(:)" (:) <*> x' <*>seqList es xs)
    where
        x' = mkVar x

parList' es xs = evalList (es `dot` rpar) xs

parList :: Strategy a -> Strategy [a]
parList _ [] = mkVar' "[]" []
parList es (x:xs) = par (es =<< x') (mkVar' "(:)" (:) <*> x' <*> (parList es xs))
    where
        x' = mkVar x

parListChunk :: Int -> Strategy a -> Strategy [a]
parListChunk n strat xs
  | n <= 1    = parList strat xs
  | otherwise = concat `fmap` parList (seqList strat) (chunk n xs)

chunk :: Int -> [a] -> [[a]]
chunk _ [] = []
chunk n xs = as : chunk n bs where (as,bs) = splitAt n xs


using :: a -> Strategy a -> TPar a
using x s = s x

rusing :: TPar a -> Strategy a -> TPar a
rusing x s = s =<< x

parMap :: Strategy b -> (a -> b) -> [a] -> TPar [b]
parMap st f xs = map f xs `using` parList st

genMap :: (Functor f, NFData b) => (Strategy b -> Strategy (f b)) -> (a -> b) -> f a -> TPar (f b)
genMap str f x = fmap f x `using` str rdeepseq 

demanding   = flip pseq
sparking    = flip par
