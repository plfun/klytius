import Klytius
import Klytius.Show  hiding (ppseq)
import Klytius.PGrafo
import Data.Reify
import qualified Control.Parallel as P
import Prelude hiding ((<*>),(<$>))

a = mkVar' "a" ()
b = mkVar' "b" ()

-------------
-- Etiquetas
-------------

etiqpar :: TPar ()
etiqpar = pars "Etiqueta Par" a b

etiqseq :: TPar ()
etiqseq = seqs "Etiqueta Seq" a b

grafet :: IO FilePath
grafet = do
    graficar False etiqpar "etqpar"
    graficar False etiqseq "etqseq"
-------------
-- Etiquetas END
-------------
---------------------------------------
-- Utilizar etiquetas
---------------------------------------

etiqparlist :: Show a => [a] -> TPar [a]
etiqparlist = foldr ( \ x xs ->
    let x' = mkVar x
    in par x' ((:) <$> x' <*> xs)) (mkVar [])

etiquetascompletas :: Show a => [a] -> TPar [a]
etiquetascompletas = foldr ( \ x xs ->
    let x' = mkVars x
    in par x' ((mkVar' "(:)" (:)) <*> x' <*> xs)) (mkVars [])

ejemploetiquetas :: IO FilePath
ejemploetiquetas = do
    let ej = map (Just ) ([2,3,4,87,4,3,2,1] :: [Int])
    graficar False (etiqparlist ej) "listsinetiq"
    graficar False (etiquetascompletas ej) "listconetiq"

--- etiquetas dinamicas
--
ssll' :: Int -> [Int] -> TPar Int
ssll' _ []  = mkVars 0
ssll' _ [x] = mkVars x
ssll' n xs  = par lf (pseq rf (lf+rf))
    where
        nn = div n 2
        (lf,rf) =   (ssll' nn (take nn xs)
                    ,ssll' nn (drop nn xs))

sl' :: Int -> [Int] -> TPar Int
sl' _ []  = mkVars 0
sl' _ [x] = mkVars x
sl' n xs  = pars "FPart" lf (seqs "SPart" rf (lf+rf))
    where
        nn = div n 2
        (lf,rf) =   (sl' nn (take nn xs)
                    ,sl' nn (drop nn xs))

sl :: [Int] -> TPar Int
sl xs = sl' nxs xs
    where
        nxs = length xs

ssll :: [Int] -> TPar Int
ssll xs = ssll' nxs xs
    where
        nxs = length xs

ejemplodin :: IO FilePath
ejemplodin = do
    let ej = ([2,3,4,1] :: [Int])
    graficar False (ssll ej) "ssll"
    graficar False (sl ej) "sslletiq"
    
---------------------------------------
-- Utilizar etiquetas END
---------------------------------------

ppar = par a b
ppseq = pseq a b

sumapar :: Int -> Int -> TPar Int
sumapar x y = let (x', y') = (mkVar' "x" x, mkVar' "y" y) in
    par x' (x' + y')

sumaparpseq :: Int -> Int -> TPar Int
sumaparpseq x y = let (x', y') = (mkVar' "x" x, mkVar' "y" y) in
    par x' (pseq y' (x' + y'))

suma :: Int -> Int -> TPar Int
suma l r = let (l',r') = (mkVars l, mkVars r) in
            par l' (pseq r' (l' + r'))

parvalval :: TPar ()
parvalval = par (mkVar ()) (mkVar ())

seqvalval :: TPar ()
seqvalval = pseq (mkVar ()) (mkVar ())

funvalval :: TPar ()
funvalval = app (mkVar (\_->())) (mkVar ())

val :: TPar ()
val = mkVar ()

funfun :: TPar ()
funfun = app (app (mkVar (\_ _ -> ())) (mkVar ())) (mkVar ())

ejredd :: TPar ()
ejredd = 
    let (l,r) = (mkVar 4, mkVar 5)
    in par (l+r) (mkVar ())

tt :: TPar ()
tt = mkVar ()

infinitus :: TPar ()
infinitus = par tt infinitus

main = do
    let s1 = suma 4 7
    putStrLn "parsuma 4 7"
    ipritty s1
    putStrLn "reifyGraph (parsuma 4 7)"
    putStrLn "Generando gráfico"
    graficar False s1 "parsuma"
    putStrLn "parvalval"
    custy False False 0.35 parvalval "parvalval"
    putStrLn "seqvalval"
    custy False False 0.35 seqvalval "seqvalval"
    putStrLn "Ejemplo Cap2"
    custy False False 0.35 ppar "parab"
    custy False False 0.35 ppseq "pseqab"
    putStrLn "SumaPar"
    custy False False 0.35 (sumapar 4 7) "sumapar"
    putStrLn "SumaParPseq"
    custy False False 0.35 (sumaparpseq 4 7) "sumaparpseq"
    putStrLn "Etiquetas"
    grafet
    putStrLn "Ejemplo Etiquetas"
    ejemploetiquetas
    putStrLn "Ejemplo Suma Etiquetas"
    ejemplodin 
