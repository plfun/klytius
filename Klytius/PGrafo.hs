{-# LANGUAGE GADTs, TypeFamilies, ExistentialQuantification, FlexibleInstances, RankNTypes #-}
module Klytius.PGrafo
    ( graficar
    , custy
    , spgraph
    , withCanv
    , genDot
    , Node
    , multigraficar 
    )
where

import Klytius
import Data.Graph.Inductive hiding (empty, Node, Graph)
import Data.GraphViz 
import qualified Data.GraphViz.Attributes.HTML as H
import Data.GraphViz.Attributes.Complete 
import Data.GraphViz.Commands
import Data.GraphViz.Commands.IO
import qualified Data.Text.Lazy as T
import Data.Reify.Graph
import Data.Reify
import Data.Map.Strict
import qualified Control.Applicative as A

------
-- Grafo
------
--
-- | Node representation for TPar.
data Node s where
    Par :: TLabel -> s -> s -> Node s --  Parallel Comp
    Seq :: TLabel -> s -> s -> Node s --  Sequential Comp
    Val :: TLabel -> Node s --  Val insertions
    deriving (Show)

instance Functor Node where
    fmap f (Par l e1 e2) = Par l (f e1) (f e2)
    fmap f (Seq l e1 e2) = Seq l (f e1) (f e2)
    fmap f (Val l) = Val l

instance MuRef (TPar s) where
    type DeRef (TPar s) = Node
    mapDeRef f (Lbl s x) | (Par' l r) <- x   = Par s A.<$> f l A.<*> f r
    mapDeRef f (Lbl s x) | (Seq' l r) <- x   = Seq s A.<$> f l A.<*> f r
    mapDeRef f (Lbl (d,v) x) | (Lbl (d',v') y) <- x   = mapDeRef f (Lbl (d++d',v++(' ':v')) y)
    mapDeRef f (Lbl s x) | (Val' _)   <- x   = A.pure (Val s)
    mapDeRef f (Par' l r)  = Par ("","") A.<$> f l A.<*> f r
    mapDeRef f (Seq' l r)  = Seq ("","") A.<$> f l A.<*> f r
    mapDeRef f (Val' _)    = A.pure (Val ("","")) 

instance Labellable TLabel where
    toLabelValue s = toLabelValue $ T.pack $ show s

data Lbl where -- Aristas
    Spark   :: Lbl
    LSeq    :: Lbl
    PExec   :: Lbl
    Exec    :: Unique -> Lbl
    Extra   :: String -> Lbl

data NLbl where -- Nodos
    NPar :: TLabel -> NLbl
    NSeq :: TLabel -> NLbl
    NVal :: TLabel -> NLbl
    NExtra :: String -> NLbl

instance Show NLbl where
    show (NPar ms)  = "Par" ++ show ms
    show (NSeq ms)  = "Seq" ++ show ms
    show (NVal ms)  = show ms
    show (NExtra ms)= 'E' : show ms

instance Show Lbl where
    show Spark      = "Spark"
    show (Exec x)   = show x ++ "->" ++ "Exec"
    show PExec      = "PExec"
    show LSeq       = "Seq"
    show (Extra s)  = s    

mkp :: (Unique, Node Unique) -> ([(Unique, NLbl)], [(Unique, Unique, Lbl)])
mkp (x, Par s l r) = ([(x, NPar s)], res)
        where
            l' = (x,l, Spark)
            r' = (x,r, PExec)
            res = [l', r'] 
            -- x -> l | x -> r
mkp (x, Seq s l r) = ([(x, NSeq s)] , res)
        where
            l' = (x,l, LSeq)
            r' = (x,r, Exec x)
--            g  = (u,l,GEdge)
            res = [l', r']
            -- x -> l, x ....> r 
mkp (u, Val s) = ([(u, NVal s)],[])

maxi :: [(Unique, b)] -> Unique -> Unique
maxi [] u = u
maxi ((u',_):xs) u | u' > u = maxi xs u'
                   | otherwise = maxi xs u

mkG :: [(Unique, Node Unique)] -> ([(Unique, NLbl)],[(Unique, Unique, Lbl)])
mkG xs = (nodes,edges) 
        where 
        (nodes, edges) = Prelude.foldl
                (\ (ru, rnu) n -> 
                    let (u, nu) = mkp n
                    in (u ++ ru, nu ++ rnu))
                ([],[])
                xs

mkG' :: [(Unique, Node Unique)] -> String -> (Unique, Int) -> ([(Unique, NLbl)],[(Unique, Unique, Lbl)])
mkG' xs name (u, base) = (nodes',edges') 
        where 
        (nombre, base') = (base+1, base+1)
        (nodes, edges) = Prelude.foldl
                (\ (ru, rnu) (u',nu') -> 
                    let (u@[_], nu) = mkp (u' + base', fmap (base'+) nu') 
                    in (u ++ ru, nu ++ rnu))
                ([],[])
                xs
        nodes' = (nombre , NExtra name) : nodes
        edges' = (nombre , u+base' , Extra "") : edges

graph :: Graph Node -> Gr NLbl Lbl 
graph (Graph netlist _) = mkGraph ns es
    where
        (ns,es) = mkG netlist

multigraph :: [(String, Graph Node)] -> Gr NLbl Lbl
multigraph xs = mkGraph ns es
    where
        (ns,es, _) = Prelude.foldl (\ (ns',es', f) (name,Graph nt st) ->
                        let (n,e) = mkG' nt name (st,f)
                        in (n ++ ns', e ++ es', f + length n )
                        ) ([],[],0) xs

shapeNods :: NLbl -> Attribute
shapeNods (NPar _)   = Shape Ellipse
shapeNods (NSeq _)   = Shape Octagon --DiamondShape
shapeNods (NVal _)   = Shape BoxShape
shapeNods (NExtra _) = Shape DoubleCircle

mkLbl :: (Show a) => a -> Attribute
mkLbl = toLabel . show 

emptyLbl :: Attribute
emptyLbl = Label $ StrLabel T.empty

labelStyle :: String -> TLabel -> Attribute
labelStyle "" ("","") = emptyLbl
labelStyle s ("","") = Label $ StrLabel $ T.pack s 
labelStyle "" ("",v) = Label $ HtmlLabel $ H.Text
                    [ H.Font [H.Color (X11Color Red)] [H.Str $ T.pack v] ]
labelStyle s ("",v)  = Label $ HtmlLabel $ H.Text
                    [ H.Font [H.Color (X11Color Red)] [H.Str $ T.pack v]
                    , H.Str $ T.pack "@"
                    , H.Str $ T.pack s]
labelStyle "" (d,"") = Label $ HtmlLabel $ H.Text
                    [ H.Font [H.Color (X11Color Green4)] [H.Str $ T.pack d] ]
labelStyle s (d,"")  = Label $ HtmlLabel $ H.Text
                    [ H.Font [H.Color (X11Color Green4)] [H.Str $ T.pack d]
                    , H.Str $ T.pack "@"
                    , H.Str $ T.pack s]
labelStyle "" (d,v)  = Label $ HtmlLabel $ H.Text
                    [ H.Font [H.Color (X11Color Green4)] [H.Str $ T.pack d]
                    , H.Str $ T.pack "|"
                    , H.Font [H.Color (X11Color Red)] [H.Str $ T.pack v]]
labelStyle s (d,v)   = Label $ HtmlLabel $ H.Text
                    [ H.Font [H.Color (X11Color Green4)] [H.Str $ T.pack d]
                    , H.Str $ T.pack "|"
                    , H.Font [H.Color (X11Color Red)] [H.Str $ T.pack v]
                    , H.Str $ T.pack "@"
                    , H.Str $ T.pack s]

edgeStyle :: Unique -> Unique -> Attribute
edgeStyle src dst = Label $ StrLabel $ T.pack (show src ++ "->" ++ show dst)

curve :: Attribute
curve = UnknownAttribute (T.pack "arrowtail") (T.pack "curve")

nodeSTD :: Bool -> (Unique, NLbl) -> Attributes
nodeSTD b (x,l@(NPar s))  = if b then
                                 [labelStyle (show x) s, shapeNods l]
                            else [labelStyle "" s, shapeNods l]
nodeSTD b (x,l@(NSeq s))  = if b then
                                 [labelStyle (show x) s, shapeNods l]
                            else [labelStyle "" s, shapeNods l]
nodeSTD b (x,l@(NVal s))  = if b then
                                 [labelStyle (show x) s, shapeNods l]
                            else [labelStyle "" s, shapeNods l]
nodeSTD b (_,l@(NExtra s)) = [labelStyle "" ("",s) , shapeNods l] 

edgeSTD :: (Unique,Unique,Lbl) -> Attributes 
edgeSTD (s,d, Spark) = [Style [SItem Dashed []]] -- ---------------
edgeSTD (s,d, LSeq)  = [Style [SItem Dotted []]] -- ...............
edgeSTD (s,d,Exec x) = [Style [SItem Bold [], SItem Solid []]] -- →
edgeSTD (s,d, Extra x) = [Style [SItem Bold [], SItem Solid []]] -- →
edgeSTD (s,d,l)      = [emptyLbl] 

grafiParams :: Bool -> Bool -> Double -> GraphvizParams Unique NLbl Lbl () NLbl
grafiParams b t sep = nonClusteredParams {
  globalAttributes = ga,
    fmtNode = nodeSTD b,
    fmtEdge = edgeSTD 
  }
  where
    ga = [
        GraphAttrs [
            RankDir (if t then FromTop else FromLeft),
            BgColor [toWColor White],
            LabelFontName $ T.pack "Helvetica-Narrow",
            NodeSep 0.1,
            Ratio AutoRatio,
            RankSep [sep]
        ],
        NodeAttrs [
            --Shape BoxShape,
            --emptyLbl, -- Sin lbl por defecto
            FillColor [toWColor White],
            LabelFontName $ T.pack "Helvetica-Narrow"
            --,Style [SItem Filled []]
        ]]

withCanv :: Bool -> TPar a -> IO ()
withCanv b g' = do
    g <- reifyGraph g' 
    quitWithoutGraphviz "No se registra la suite GraphViz instalada en su sistema."
    runGraphvizCanvas Dot (graphToDot (grafiParams b False 0.25) (graph g)) Xlib

spgraph :: Bool -> Graph Node -> String -> IO FilePath
spgraph b x s = do
    quitWithoutGraphviz "No se registra la suite GraphViz instalada en su sistema."
    runGraphvizCommand Dot (graphToDot (grafiParams b False 0.25) (graph x)) Pdf (s++".pdf")

multispgraph :: Bool -> [(String, Graph Node)] -> String -> IO FilePath
multispgraph b xs s = do
    quitWithoutGraphviz "No se registra la suite GraphViz instalada en su sistema."
    runGraphvizCommand Dot (graphToDot (grafiParams b False 0.25) (multigraph xs)) Pdf (s++".pdf")

custy
    :: Bool             -- ^ Identificador de nodos interno.
    -> Bool             -- ^ Left -> Right | Top -> Bottom
    -> Double           -- ^ Distancia de rangos.
    -> TPar a           -- ^ Grafo a gráficar.
    -> String           -- ^ Nombre del archivo resultante.
    -> IO FilePath
custy b t d x s = do
    g <- reifyGraph x
    quitWithoutGraphviz "No se registra la suite GraphViz instalada en su sistema."
    runGraphvizCommand Dot (graphToDot (grafiParams b t d) (graph g)) Pdf (s++".pdf")

genDot :: Bool -> TPar a -> String -> IO FilePath
genDot b g' s = do
    g <- reifyGraph g'
    runGraphviz (graphToDot (grafiParams b False 0.25) (graph g)) DotOutput (s++".dot")

graficar :: Bool -> TPar a -> String -> IO FilePath
graficar b g s = do
    g' <- reifyGraph g
    spgraph b g' s

multigraficar :: Bool -> [(String, TPar a)] -> String -> IO FilePath
multigraficar b gs s = do
    gs' <- mapM (\ (s, g) -> do
            g' <- reifyGraph g
            return (s, g')) gs
    multispgraph b gs' s   

