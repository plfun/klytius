module Klytius.Show 
    ( noLbl
    , pritty 
    , ipritty)
where

import Klytius
import Data.Reify
import Data.Reify.Graph
import Data.Map.Strict
import Text.PrettyPrint
import Control.Monad.State

-- Mostramos el 'esqueleto' de par y seqs
-- Sin imprimir que sin observar los valores a paralelizar.

instance Show (TPar a) where
    show (Par' l r) = "Par:("++ show l++") ("++show r++")" 
    show (Seq' l r) = "Seq:("++ show l++") ("++show r++")" 
    show (Lbl  s x) = "Lbl:" ++ show s ++ " " ++ show x
    show (Val' _)   = "Val"

noLbl :: TPar a -> String
noLbl (Par' l r) = "Par (" ++ noLbl l ++  ") (" ++ noLbl r ++ ")"
noLbl (Seq' l r) = "Seq (" ++ noLbl l ++  ") (" ++ noLbl r ++ ")"
noLbl (Lbl (s1,s2) (Lbl (r1,r2) x)) = noLbl (Lbl (s1++r1,s2++r2) x)
noLbl (Lbl s (Val' _)) = "Val " ++ snd s
noLbl (Lbl _ x)  = noLbl x
noLbl (Val' _) = "Val _"

--ntoStr :: Show s => Node s -> String
--ntoStr (Val s) = "V:"++s
--ntoStr (Par s l r) = "Par("++ s ++ ":" ++ show l ++ ","++ show r ++")"
--ntoStr (Seq s l r) = "Seq("++ s ++ ":" ++ show l ++ ","++ show r ++")"

pppar :: Doc
pppar = text "Par:"

ppseq :: Doc
ppseq = text "Seq:"

ppapp :: Doc
ppapp  = text "App:"

line :: Doc
line = text "\n"

pipe :: Doc
pipe = char '|'

gg :: Doc
gg = char '-'

fl :: Doc
fl = char '>'

tuniq :: Unique -> Doc
tuniq = text . show

printing :: Int -> TPar a -> Doc
printing n (Val' _)   = text "Val"
printing n (Par' a b) = pppar $+$ nest (n+1) (pipe <> printing (n+1) a) $+$ nest (n+1) (pipe <> printing (n+1) b)
printing n (Seq' a b) = ppseq $+$ nest (n+1) (gg <> printing (n+1) a) $+$ nest (n+1) (fl <> printing (n+1) b)
printing n (Lbl  s x) = text (show s) <+> (lparen <> printing n x <> rparen)
                       
pritty :: TPar a -> String
pritty = render . printing 0

ipritty :: TPar a -> IO ()
ipritty = putStrLn . pritty

--tick :: State Int Int
--tick = do
--    l <- get
--    let nl = l+1
--    put nl
--    return nl
--
--printingC :: Unique -> Map Unique (Node Unique) -> State Int Doc
--printingC u m | (Val "")  <- m!u = return (quotes $ (text "Val" <+> tuniq u))
--printingC u m | (Val s)   <- m!u = return (quotes $ (text "Val" <+> tuniq u <+> text s))
--printingC u m | (Par s l r) <- m!u = do
--    n <- tick
--    l' <- printingC l m
--    r' <- printingC r m
--    return ((pppar <+> tuniq u) $+$ (nest n ((text s <+> l') $+$ r')))
--printingC u m | (Seq s l r) <- m!u = do
--    n <- tick
--    l' <- printingC l m
--    r' <- printingC r m
--    return ((ppseq <+> tuniq u) $+$ (nest n ((text s <+> l') $+$ r')))
--
--printingComps :: InComp -> String
--printingComps (u,m) = render $ evalState (printingC u m) 0
--
--iprintingComps :: InComp -> IO ()
--iprintingComps = putStrLn . printingComps 
