# Klytius
--------------------------------------------------------------------------------

### Install

Requirements:

    * GHC
    * GraphViz

Install with `cabal install`. Once the library is installed you just need to
import it.

To just import the EDSL `import Klytius`.

The graphic functionalities are in `import Klytiues.PGrafo`

A basic pritty printing is in `import Klytius.Show`

### Examples

There are some examples in the *Ejemplos* folder.

### Instalación

Requisitos:

    *Tener instalado Cabal, y GHC.
    *El paquete GraphViz.

La instalación se realiza de manera automática por
parte de Cabal, dentro de la carpeta donde se encuentran
los archivos del repositorio:

    
```
#!bash
    cabal install -j
```

Una vez instalada la libreria, simplemente importar los modulos necesario.

Modulo donde se encuentra el EDSL,

```
#!haskell
    import Klytius
```

Modulo que provee las funciones necesarias para la generación de los gráficos.

```
#!haskell
    import Klytius.PGrafo
```

Modulo que provee un *pritty printing* para el EDSL.

```
#!haskell
    import Klytius.Show
```

### Ejemplos

Dentro de la carpeta Ejemplos se encuentran algunos ejemplos de como utilizar el EDSL, y cómo utilizar el modulo Klytius.PGrafo.

Una vez instalada la libreria Klytius, los diferentes ejemplos pueden ser compilados. Para compilar el ejemplo *realMR* es necesario instalar el paquete **split** ( *cabal install split* )
